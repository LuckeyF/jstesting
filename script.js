console.log("defered!");

let superDiv = document.getElementById("superDiv");

function myNamedFunction(event) {
	console.log("event", event);
	console.log("clicked!");
}

function newFunc() {
	console.log("new func");
}

superDiv.addEventListener("click", myNamedFunction);
superDiv.addEventListener("click", newFunc);


superDiv.removeEventListener("click", myNamedFunction);


//___________________________________________________________
let myLis = document.querySelectorAll("li");

// Array.from(myLis); // Um manuell ein Array aus einer NodeList zu erstellen


//___________________________________________________________

let counter = document.getElementById('counter');
document.getElementById('my-button').addEventListener('click', function () {
// So wird der Text auf der Webseite verändert
	counter = parseInt(counter) + 1;
	//Next line fails silently if the line before are still active (not commented out)
	counter.innerText = parseInt(counter.innerText) + 1;
});


//___________________________________________________________

let mainDiv = document.getElementById("mainDiv");

let apples1 = document.querySelectorAll(".apple");
let apples2 = mainDiv.querySelectorAll(".apple");

console.log("apples1", apples1);
console.log("apples2", apples2);


//___________________________________________________________

let myForm = document.querySelector("form");

myForm.addEventListener("submit", function (event) {
	event.preventDefault();
	console.log("submit");
});

let myATag = document.getElementById("myATag");


//___________________________________________________________

let personTable = document.getElementById("personTable");


document.getElementById("tableForm").addEventListener("submit", function (event) {
	event.preventDefault();

	let vn = document.getElementById("vn").value;
	let nn = document.getElementById("nn").value;
	let gb = document.getElementById("gb").value;

	let newPerson = {
		vn,
		nn,
		gb
	};

	let existingPersons = JSON.parse(localStorage.getItem("persons"));

	if(existingPersons?.length){
		localStorage.setItem("persons", existingPersons.push(newPerson));
	}

});

let existingPersons = JSON.parse(localStorage.getItem("persons"));

console.log(existingPersons);

existingPersons?.forEach(function (person) {
	let tr = document.createElement("tr");

	let tdVN = document.createElement("td");
	tdVN.innerText = person.vn;

	let tdNN = document.createElement("td");
	tdNN.innerText = person.nn;

	let tdGB = document.createElement("td");
	tdGB.innerText = person.gb;

	tr.appendChild(tdVN);
	tr.appendChild(tdNN);
	tr.appendChild(tdGB);

	personTable.appendChild(tr);
});


